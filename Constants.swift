//
//  Constants.swift
//  CryptoCrypto
//
//  Created by Ozgur Toprak on 16.02.2018.
//  Copyright © 2018 Ozgur Toprak. All rights reserved.
//

import Foundation

let BASE_URL_STRING = "https://api.coinmarketcap.com/v1/ticker/?limit=300"
let COIN_DATA_URL_STRING = "https://min-api.cryptocompare.com/data/all/coinlist"
let ADMOB_APP_ID = "ca-app-pub-4346151946219445~4137215787" 
let ADMOB_APP_UNIT_ID = "ca-app-pub-4346151946219445/6227399911"


 let MY_IPHONE_DEVICE_TEST_ID = "5a3cdb2301592e537eab05441b4842b6"

enum UnitOfCoin : String {
    case BTC = "BTC"
    case ETH = "ETH"
    case USD = "USD"
    case EUR = "EUR"
}
