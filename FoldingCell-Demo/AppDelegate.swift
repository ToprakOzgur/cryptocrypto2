  

import UIKit
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var delegate: MainTableViewController?
    
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {
        

        GADMobileAds.configure(withApplicationID: ADMOB_APP_ID)

        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        delegate?.getExchangesData()
        delegate?.setup()
        delegate?.isFirstStart = false
        
    }
}
