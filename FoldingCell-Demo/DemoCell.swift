

import FoldingCell
import UIKit

class DemoCell: FoldingCell {

    @IBOutlet weak  var currencySymbol: UILabel!
    
    @IBOutlet weak var currencyImage: UIImageView!
  
    @IBOutlet weak var currencyName: UILabel!
    
    @IBOutlet weak var currencySymbolInside: UILabel!
    
    @IBOutlet weak var priceBTC: UILabel!
    
    @IBOutlet weak var priceUSD: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var priceUnitLabel: UILabel!
    
    @IBOutlet weak var changeLabel: UILabel!
    
    @IBOutlet weak var changeIn1HLabel: UILabel!
    
    @IBOutlet weak var changeIn24hLabel: UILabel!
    
    @IBOutlet weak var changeIn7dLabel: UILabel!

    @IBOutlet weak var favoritesButton: UIButton!
    
    @IBOutlet weak var favoriteStarIcon: UIImageView!
    
    
    weak var controller : UIViewController!
    
    override func awakeFromNib() {
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        super.awakeFromNib()
    }

    override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
   
}

// MARK: - Actions ⚡️

extension DemoCell {

    @IBAction func buttonHandler(_: AnyObject) {
        guard let symbol = currencySymbol.text else {return}
        
        if Favorites.favorites.contains(symbol) {
            Favorites.removeFromFavorites(coinName: symbol)
            favoritesButton.setTitle("Add to Favorites", for: .normal)
            favoriteStarIcon.isHidden = true
            
            if let cont = controller as? FavoritesTableViewController {
             
              
                cont.favoritesTableView.reloadRows(at: [cont.favoritesTableView.indexPathForSelectedRow!], with: .right)
                 cont.setup()
            }
          
        } else {
            Favorites.addToFavorites(coinName: symbol)
            favoritesButton.setTitle("Remove from Favorites", for: .normal)
            favoriteStarIcon.isHidden = false
        }
        
    }
}
