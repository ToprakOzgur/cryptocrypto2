//
//  FavoritesTableViewController.swift
//  FoldingCell-Demo
//
//  Created by Ozgur Toprak on 14.03.2018.
//  Copyright © 2018 Alex K. All rights reserved.
//

import UIKit
import FoldingCell

class FavoritesTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var favoritesTableView: UITableView!
    
    let kCloseCellHeight: CGFloat = 60
    let kOpenCellHeight: CGFloat = 240
    let kRowsCount = 300
    var cellHeights: [CGFloat] = []
    
    var coinsArray : [Currency] = [] {
        didSet {
            
            favoritesTableView.reloadData()
        }
    }
    
    let operations = Operations()
    
    var allMarketsCoins = [String : Any]()
    
   
    
    override func viewDidLoad() {
        
        favoritesTableView.delegate = self
        favoritesTableView.dataSource = self
        navigationItem.title = "Favorites"
        
 }
    
    override func viewWillAppear(_ animated: Bool) {
        if let tab = tabBarController! as? StartTabBarController {
            tab.showAds()
        }
         setup()
    }
    
    func setup() {
    
        cellHeights = Array(repeating: kCloseCellHeight, count: kRowsCount)
        favoritesTableView.estimatedRowHeight = kCloseCellHeight
        favoritesTableView.rowHeight = UITableViewAutomaticDimension
        favoritesTableView.backgroundColor = UIColor.black
        
        
        if let nav = tabBarController!.viewControllers![0] as? UINavigationController , let favs = nav.viewControllers[0] as? MainTableViewController {
            
                coinsArray = favs.coinsArray.filter{Favorites.favorites.contains($0.symbol!)}
            
        }
    }
}

// MARK: - TableView

extension FavoritesTableViewController {
    
     func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return coinsArray.count
    }
    
     func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as DemoCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == kCloseCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        //TODO: cell /currency infos
        
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath) as! DemoCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        
        cell.currencySymbol.text = coinsArray[indexPath.row].symbol
        cell.currencySymbolInside.text = coinsArray[indexPath.row].symbol
        cell.currencyName.text = coinsArray[indexPath.row].name
        cell.priceBTC.text =  coinsArray[indexPath.row].price_btc
        cell.priceUSD.text =  coinsArray[indexPath.row].price_usd
        cell.priceLabel.text =  coinsArray[indexPath.row].symbol == "BTC" ? coinsArray[indexPath.row].price_usd : coinsArray[indexPath.row].price_btc
        cell.priceUnitLabel.text =  coinsArray[indexPath.row].symbol != "BTC" ? "BTC" : "﹩"
        
        if let symbol = coinsArray[indexPath.row].symbol {
            if Favorites.favorites.contains(symbol) {
                cell.favoritesButton.setTitle("Remove from Favorites", for: .normal)
                  cell.favoriteStarIcon.isHidden = false
            } else {
                cell.favoritesButton.setTitle("Add to Favorites", for: .normal)
                  cell.favoriteStarIcon.isHidden = true
            }
            
        }
        
        setupChangeLabelsAtCell(cell: cell, indexpath: indexPath)
        cell.controller = self
        return cell
        
    }
    
    func setupChangeLabelsAtCell(cell: DemoCell, indexpath: IndexPath)  {
        
        if let change = coinsArray[indexpath.row].percent_change_24h {
            
            if let changeInteger = Double(change) {
                
                cell.changeLabel.text = "\(changeInteger)" + "%"
                cell.changeIn24hLabel.text = "\(changeInteger)" + "%"
                
                if changeInteger >= 0 {
                    cell.changeLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                    cell.changeIn24hLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                } else {
                    cell.changeLabel.textColor = UIColor.red
                    cell.changeIn24hLabel.textColor = UIColor.red
                }
                
            }
        }
        
        if let change = coinsArray[indexpath.row].percent_change_7d {
            
            if let changeInteger = Double(change) {
                
                
                cell.changeIn7dLabel.text = "\(changeInteger)" + "%"
                
                if changeInteger >= 0 {
                    
                    cell.changeIn7dLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                } else {
                    
                    cell.changeIn7dLabel.textColor = UIColor.red
                }
                
            }
        }
        
        if let change = coinsArray[indexpath.row].percent_change_1h {
            
            if let changeInteger = Double(change) {
                
                
                cell.changeIn1HLabel.text = "\(changeInteger)" + "%"
                
                if changeInteger >= 0 {
                    
                    cell.changeIn1HLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                } else {
                    
                    cell.changeIn1HLabel.textColor = UIColor.red
                }
                
            }
        }
        
        
    }
    
     func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FoldingCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == kCloseCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = kOpenCellHeight
            cell.unfold(true, animated: true, completion: nil)

            
            
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = kCloseCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }

    
    
}
