

import FoldingCell
import UIKit
import GoogleMobileAds

class MainTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    @IBOutlet weak var tableviewMain: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let kCloseCellHeight: CGFloat = 60
    let kOpenCellHeight: CGFloat = 240
    let kRowsCount = 300
    var cellHeights: [CGFloat] = []
    
    var coinsArray : [Currency] = [] {
        didSet {
            tableviewMain.reloadData()
            
        }
        
    }
    
    var swapForCurrencyArray = [Currency]()
    
    var coindataResult = AllCoinData()
    
    let operations = Operations()
    
    var allMarketsCoins = [String : Any]()
    
    var isFirstStart = true
    var isSearching = false
    
    var refreshControl = UIRefreshControl()
    
    var interstitial: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.delegate = self
        tableviewMain.delegate = self
        tableviewMain.dataSource = self
        searchBar.delegate = self
          navigationItem.title = "Coins"
        // getCoinData()
        
        refreshControl.addTarget(self, action: #selector(self.refreshData), for: UIControlEvents.valueChanged)
        if #available(iOS 10.0, *) {
            tableviewMain.refreshControl = refreshControl
        } else {
            tableviewMain.addSubview(refreshControl)
        }
        
        interstitial = createAndLoadInterstitial()
      
   }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFirstStart {return}
        getExchangesData()
        setup()
    }
    
    
    func setup() {
        print("setting up")
        Favorites.InitFavorites()
        cellHeights = Array(repeating: kCloseCellHeight, count: kRowsCount)
        tableviewMain.estimatedRowHeight = kCloseCellHeight
        tableviewMain.rowHeight = UITableViewAutomaticDimension
        tableviewMain.backgroundColor = UIColor.black
        
        
    }
    
    func getExchangesData() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        operations.getExchanges { [weak self] (currencies) in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.coinsArray = currencies
            }
            
        }
    }
    
    func   getCoinData(){
        operations.getCoinData { [weak self] (coinData) in
            DispatchQueue.main.async {
                //  self?.coinsArray = currencies
                
                self?.coindataResult = coinData
                
            }
        }
    }
    
    func getIcons(cell: DemoCell){
        guard    let coinSymbol = cell.currencySymbol.text else {return}
        
        let  readImage =  operations.ReadImage(coinSymbol: coinSymbol)
        
        if let img = readImage {
            
            cell.imageView?.image = img
            
        } else {
            
            guard let baseUrl = coindataResult.BaseImageUrl else {return}
            
            guard let cData =  coindataResult.Data[coinSymbol] , let url = cData.ImageUrl  else {return}
            
            operations.getCoinSymbolImage( from: (baseUrl + url), to: cell.currencyImage, coinName: coinSymbol )
        }
        
    }
    
    @IBAction func refreshButtonPressed(_ sender: UIBarButtonItem) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        refreshData()
    }
    
    @objc func refreshData(){
        showInterstitial()
        
        print("refresing")
        operations.getExchanges { [weak self] (currencies) in
            DispatchQueue.main.async {
                self?.tableviewMain.reloadData()
                self?.refreshControl.endRefreshing()
                self?.coinsArray = currencies
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                
                print("refresh end")
            }
            
        }
    }
    
    
 
}


// MARK: - TableView

extension MainTableViewController {
    
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return  coinsArray.count
    }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as DemoCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == kCloseCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath) as! DemoCell
        
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        
        cell.currencySymbol.text = coinsArray[indexPath.row].symbol
        cell.currencySymbolInside.text = coinsArray[indexPath.row].symbol
        cell.currencyName.text = coinsArray[indexPath.row].name
        cell.priceBTC.text =  coinsArray[indexPath.row].price_btc
        cell.priceUSD.text =  coinsArray[indexPath.row].price_usd
        cell.priceLabel.text =  coinsArray[indexPath.row].symbol == "BTC" ? coinsArray[indexPath.row].price_usd : coinsArray[indexPath.row].price_btc
        cell.priceUnitLabel.text =  coinsArray[indexPath.row].symbol != "BTC" ? "BTC" : "﹩"
        
        if let symbol = coinsArray[indexPath.row].symbol {
            if Favorites.favorites.contains(symbol) {
                cell.favoritesButton.setTitle("Remove from Favorites", for: .normal)
                cell.favoriteStarIcon.isHidden = false
            } else {
                cell.favoritesButton.setTitle("Add to Favorites", for: .normal)
                cell.favoriteStarIcon.isHidden = true
            }
            
        }
        
        setupChangeLabelsAtCell(cell: cell, indexpath: indexPath)
        
        
        
        return cell
        
    }
    
    func setupChangeLabelsAtCell(cell: DemoCell, indexpath: IndexPath)  {
        
        if let change = coinsArray[indexpath.row].percent_change_24h {
            
            if let changeInteger = Double(change) {
                
                cell.changeLabel.text = "\(changeInteger)" + "%"
                cell.changeIn24hLabel.text = "\(changeInteger)" + "%"
                
                if changeInteger >= 0 {
                    cell.changeLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                    cell.changeIn24hLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                } else {
                    cell.changeLabel.textColor = UIColor.red
                    cell.changeIn24hLabel.textColor = UIColor.red
                }
                
            }
        }
        
        if let change = coinsArray[indexpath.row].percent_change_7d {
            
            if let changeInteger = Double(change) {
                
                
                cell.changeIn7dLabel.text = "\(changeInteger)" + "%"
                
                if changeInteger >= 0 {
                    
                    cell.changeIn7dLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                } else {
                    
                    cell.changeIn7dLabel.textColor = UIColor.red
                }
                
            }
        }
        
        if let change = coinsArray[indexpath.row].percent_change_1h {
            
            if let changeInteger = Double(change) {
                
                
                cell.changeIn1HLabel.text = "\(changeInteger)" + "%"
                
                if changeInteger >= 0 {
                    
                    cell.changeIn1HLabel.textColor = #colorLiteral(red: 0.05013967122, green: 0.5751139323, blue: 0.1504425137, alpha: 1)
                } else {
                    
                    cell.changeIn1HLabel.textColor = UIColor.red
                }
                
            }
        }
        
        
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FoldingCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == kCloseCellHeight
        if cellIsCollapsed {
            
            cellHeights[indexPath.row] = kOpenCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            
            cellHeights[indexPath.row] = kCloseCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
    
    
    
}

extension MainTableViewController : UISearchBarDelegate {
    
    @IBAction func searchButtonPressed(_ sender: UIBarButtonItem) {
        if isSearching {
            endSearching()
            
        } else {
            searchBar.text = ""
            searchBar.becomeFirstResponder()
            swapForCurrencyArray = coinsArray
            isSearching = true
            self.searchBarHeight.constant = 50
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            coinsArray = swapForCurrencyArray
            
        } else {
            coinsArray = swapForCurrencyArray.filter({ (coin) -> Bool in
                guard let text = searchBar.text?.lowercased() else { return false }
                guard let symbol = coin.symbol else { return false }
                return symbol.lowercased().contains(text)
            })
            tableviewMain.reloadData()
        }
        
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        endSearching()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        endSearching()
    }
    
    fileprivate func endSearching() {
        searchBar.resignFirstResponder()
        isSearching = false
        coinsArray = swapForCurrencyArray
        tableviewMain.reloadData()
        self.searchBarHeight.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

extension MainTableViewController : GADInterstitialDelegate {
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func showInterstitial() {
        
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
}
