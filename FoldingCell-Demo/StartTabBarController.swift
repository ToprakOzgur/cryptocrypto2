//
//  StartTabBarController.swift
//  FoldingCell-Demo
//
//  Created by Ozgur Toprak on 28.03.2018.
//  Copyright © 2018 Alex K. All rights reserved.
//

import UIKit
import  GoogleMobileAds

class StartTabBarController: UITabBarController, GADInterstitialDelegate {
    
    var interstitial: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createInterstitialAds()
    }
    
    fileprivate func createInterstitialAds() {
        
        interstitial = GADInterstitial(adUnitID: ADMOB_APP_UNIT_ID)
        interstitial.delegate = self
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID, MY_IPHONE_DEVICE_TEST_ID ]
        interstitial.load(request)
    }
    
    func showAds() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
            createInterstitialAds()
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
        createInterstitialAds()
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}
