//
//  AllCoinData.swift
//  FoldingCell
//
//  Created by Ozgur Toprak on 16.03.2018.
//  Copyright © 2018 Alex K. All rights reserved.
//

import Foundation

struct AllCoinData : Decodable {
    
    var BaseImageUrl: String?
    var Data = [String: CryptoCompareCoin]()
}
