//
//  Favorites.swift
//  FoldingCell
//
//  Created by Ozgur Toprak on 14.03.2018.
//  Copyright © 2018 Alex K. All rights reserved.
//

import Foundation
class Favorites {
    
    static var favorites = Set<String>()

    
    static func InitFavorites() {

     let    array = UserDefaults.standard.object(forKey: "favorites")  as? [String] ?? [String]()
         favorites = Set(array)
        
    }
    
    static func addToFavorites(coinName coin: String){
        favorites.insert(coin)
        let array = Array(favorites)
        UserDefaults.standard.set(array, forKey: "favorites")
        print(favorites)
    }
    
    static func removeFromFavorites(coinName coin: String){
        favorites.remove(coin)
        let array = Array(favorites)
        UserDefaults.standard.set(array, forKey: "favorites")
        print(favorites)
    }
}
