//
//  Operations.swift
//  CryptoCrypto
//
//  Created by Ozgur Toprak on 16.02.2018.
//  Copyright © 2018 Ozgur Toprak. All rights reserved.
//

import Foundation
import UIKit

class Operations {
    
    
    func getExchanges(complationHandler complation: @escaping ([Currency]) ->Void) {
        
        guard  let url = URL(string: BASE_URL_STRING) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error)  in
            
            guard let data = data else {return}
            
            do {
                let res = try JSONDecoder().decode([Currency].self, from: data)
                complation(res)
                
            }
            catch let jsonnErr {
                print("Error seriaalizin json:",jsonnErr)
            }
            
            }.resume()
        
        
    }
    
    func getCoinData(complitionHandler complition: @escaping (AllCoinData ) -> Void ) {
        
        guard let url = URL(string: COIN_DATA_URL_STRING) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do {
                let res = try JSONDecoder().decode(AllCoinData.self, from: data)
                complition(res)
                
            }
            catch let jsonnErr {
                print("Error seriaalizin json:",jsonnErr)
            }
            
            }.resume()
        
    }
    
    
    func getCoinSymbolImage(from url: String,to imageView : UIImageView,  coinName coin : String){
        
    
        guard  let url = URL(string: url) else {return }
        
        var img :UIImage?
        DispatchQueue.global().async {
            
            do {
                let data = try  Data(contentsOf: url)
                DispatchQueue.main.async {
                    imageView.image = UIImage(data: data)
                    img = imageView.image
                    self.saveImg(img: img, coinSymbol: coin)
                }
                
            } catch let jsonnErr{
                print("Error seriaalizin json:",jsonnErr)
            }
        }
  }
    
    func saveImg(img :UIImage?, coinSymbol coin : String){
        
        do {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileURL = documentsURL.appendingPathComponent("\(coin).png")
            if let pngImageData = UIImagePNGRepresentation(img!) {
                try pngImageData.write(to: fileURL, options: .atomic)
                print("save etti")
            }
        } catch { }
    }
    
    func ReadImage(coinSymbol coin : String) -> UIImage? {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent("\(coin).png").path
        if FileManager.default.fileExists(atPath: filePath) {
            print("\(coin) okuyor, image var ")
            return UIImage(contentsOfFile: filePath)
        }
        print("\(coin) okuyor, image yok ")
        return nil
    }
}



